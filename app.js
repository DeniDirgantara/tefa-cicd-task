const { response } = require('express');
var express = require('express');

var app = express();

const PORT = process.env.PORT || 8080;

var router = express.Router();

router.get('/', function(req, res){
    response.send('Welcome Tefa BE');
});

router.get('/student', function(req, res){
    response.send('Welcome, Student');
});

router.get('/teacher', function(req, res){
    response.send('Welcome, Teacher');
});

app.use('/', router);

app.listen(PORT, function(){
    console.log('Listening on port' + PORT);
});